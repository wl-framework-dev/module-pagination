<nav class="pagination">
	<ul>
		<?php if( $this->has_prev ): ?>
		<li class="prev"><a href="<?=$this->link($this->add(-1))?>">Vorige</a></li>
		<?php endif; ?>
		<?php for( $p=$min; $p<=$max; $p++ ): ?>
			<?php if( $p==$current ): ?>
				<li class="cur"><a href="<?=$this->link($p)?>"><?=$p?></a></li>
			<?php else: ?>
				<li><a href="<?=$this->link($p)?>"><?=$p?></a></li>
			<?php endif; ?>
		<?php endfor; ?>
		<?php if( $this->has_next ): ?>
		<li class="next"><a href="<?=$this->link($this->add(1))?>">Volgende</a></li>
		<?php endif; ?>
	</ul>
</nav>