<?php

	namespace Pagination;

	use WebLab_Parser_URL as URL;
	use Redirect\Redirect;

	class WithView extends \WebLab_Template {

		protected $_param = 'page';

		public $page = 1;

		public $has_next = false;

		public $has_prev = false;

		public $page_size = 10;

		public $total_count = PHP_INT_MAX;

		public $offset = 0;

		public $url = BASE;

		public function __construct( $template, $page_size=10, $total_count=PHP_INT_MAX, $url_parameter='page' ) {
			parent::__construct( $template );

			$this->_param = $url_parameter;
			$this->url = BASE . trim( URL::getForRequest()->getCurrent(array($url_parameter)), '/' );
			$this->total_count = $total_count;
			$this->page_size = $page_size;
			$this->_detect();
			$this->reconfigure();
		}

		protected function _detect() {
			$param = URL::getForRequest()->getParameters();

			if( !isset( $param[$this->_param] ) ) {
				$this->page = 1;
				return;
			}

			$page = $param[$this->_param];
			if( !is_numeric( $page ) ) {
				$this->page = 1;
				return;
			}

			$this->page = (int)$page;
		}

		public function reconfigure() {
			$page = $this->add(0);
			$offset = $this->total_count;

			$offset = ($page-1) * $this->page_size;

			if( $page != $this->page )
				Redirect::go( $this->link( $page ) );

			$this->offset = $offset;
			$this->has_prev = ( $page-1 > 0 );
			$this->has_next = ( $this->page * $this->page_size < $this->total_count );
		}

		public function link( $page_number ) {
			return $this->url . '/' . $this->_param . '/' . $page_number;
		}

		public function add( $count ) {
			$page = $this->page + $count;

			if( $page < 1 )
				return 1;
			
			$max = floor( $this->total_count / $this->page_size ) + 1;
			if( $page > $max )
				return $max;

			return $page;
		}

	}