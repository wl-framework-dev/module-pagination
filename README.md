# Pagination module

This module will assist you with paging results.

## Installation

Clone this project in you `Modules` folder as `Pagination`

```shell
git clone git@bitbucket.org:wl-framework-dev/module-pagination.git Pagination
```

## Usage

```php
<?php
	$instance = new Pagination\WithView( 'Pagination\pagination/default', $per_page );

	$items = Table_Item::getInstance()->findAll(
		$pagination->page_size, $pagination->offset, $pagination->total_count );
	$pagination->reconfigure();
```